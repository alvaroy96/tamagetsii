![](https://gitlab.com/Baeolian/tamagetsii/raw/master/src/images/Logo.png)

### Cómo comenzar a trabajar

1. Haz un ***fork*** del proyecto
2. Dale a **Clone** en el ***fork*** que has creado y copia el link de **Clone with HTTPS**
3. Desde la linea de comandos: **git clone ...**
4. En **Eclipse** (con Java JRE 13) dale a la opción de **Import...**
5. Despliega **General** y selecciona **Existing Projects into Workspace**

> ¡Importante: si se trabaja con otra versión distinta a la de Java 13, no añadir ni hacer commit del archivo **.classpath** !

----

### Tareas a realizar

##### Fase 0
- [x] Iniciar el repositorio
- [x] Comprender el código
    - [x] Ayuso Martínez, Álvaro
    - [x] Cote Llamas, José
    - [x] Iglesias Trujillo, Carmen
    - [x] Pérez Peña, Antonio

##### Fase 1
- [x] Organizar proyecto por clases en distintos archivos
(Actualmente, todo está en *Main.java*)
    - [x] AnimationFrame.java
    - [x] Logic.java
    - [x] Animation.java
    - [x] Animations.java
    - [x] Components.java

##### Fase 2
- [ ] Búsqueda de bugs (descubrir y reportar)
- [ ] Bug fixing
    - [x] UI: Desplazado Texto de STATS
    - [ ] UI: Desplazado Círculo de Botón Izquierdo
    - [ ] ????

##### Fase 3 (opcional)
- [ ] Mejorar interfaz gráfica
    - [ ] Indicadores
    - [ ] Botones
- [ ] Reformatear código

> Se rechazará todo **MR** que varíe la estructura de archivos del repositorio
